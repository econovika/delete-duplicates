module Main where

import Options.Applicative
import Data.Semigroup                 ((<>))
import Data.ByteArray                 (ByteArrayAccess)
import qualified Data.ByteArray as BA (unpack)
import Crypto.Hash                    (Digest, hash)
import Crypto.Hash.Algorithms         (SHA256(..))
import Data.ByteString as BS          (readFile, ByteString(), pack)
import System.Directory               (doesDirectoryExist, getDirectoryContents, removeFile)
import System.FilePath.Posix          ((</>))
import Control.Monad                  (forM)


type File = (BS.ByteString, FilePath)

data Options = Options { dir     :: String
                       , dry_run :: Bool  } deriving (Show)

options :: Parser Options
options = Options
              <$> strOption ( long "dir"
                              <> metavar "TARGET"
                              <> help "Directory to find duplicates and delete")
              <*> switch    ( long "dry-run"
                              <> help "Only display duplicates if set")

hashStr bs = hash bs :: Digest SHA256

toByteStr :: ByteArrayAccess a => a -> ByteString
toByteStr = BS.pack . BA.unpack

byteStr :: ByteString -> ByteString
byteStr bs = toByteStr $ hashStr bs

genHash :: FilePath -> IO [File]
genHash path = do
    BS.readFile path >>= \bs -> return [(byteStr bs, path)]

getFiles :: FilePath -> IO [File]
getFiles dir = do
    names <- getDirectoryContents dir
    let sub_dirs = filter (`notElem` [".", ".."]) names
    files <- forM sub_dirs $ \path -> do
                 let path' = dir </> path
                 exists <- doesDirectoryExist path'
                 if exists
                     then getFiles path'
                     else genHash  path'
    return $ concat files

sameHash :: [File] -> IO ()
sameHash [] = return ()
sameHash ((hash, name):xs) = do
    case lookup hash xs of
        (Just name_dupl) -> putStrLn name_dupl >> putStrLn name >> sameHash xs
        (_)              -> sameHash xs

dropHash :: [File] -> IO ()
dropHash [] = return ()
dropHash ((hash, name):xs) = do
    case lookup hash xs of
        (Just name_dupl) -> removeFile name_dupl >> dropHash xs
        (_)              -> dropHash xs

findDuplicates :: FilePath -> Bool -> IO ()
findDuplicates dir flag = do
    exists <- doesDirectoryExist dir
    if exists && flag
        then getFiles dir >>= sameHash
    else if exists
        then getFiles dir >>= dropHash
    else putStrLn "Directory does not exist"

main :: IO ()
main = do
    options <- execParser parseOpts
    case options of
        Options {dir = _:_, dry_run = False}  -> findDuplicates (dir options) False
        _                                     -> findDuplicates (dir options) True
    where
        parseOpts = info (helper <*> options)
            ( fullDesc
              <> progDesc "Delete duplicate files in given directory or show them")
